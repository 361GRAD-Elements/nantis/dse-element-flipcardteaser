<?php

namespace Dse\ElementsBundle\ElementFlipcardteaser\ContaoManager;

use Contao\ManagerPlugin\Bundle\Config\BundleConfig;
use Contao\ManagerPlugin\Bundle\BundlePluginInterface;
use Contao\ManagerPlugin\Bundle\Parser\ParserInterface;
use Dse\ElementsBundle\ElementFlipcardteaser;
use Contao\CoreBundle\ContaoCoreBundle;

class Plugin implements BundlePluginInterface
{
    public function getBundles(ParserInterface $parser)
    {
        return [
            BundleConfig::create(ElementFlipcardteaser\DseElementFlipcardteaser::class)
                ->setLoadAfter([ContaoCoreBundle::class])
        ];
    }
}
